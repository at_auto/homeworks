import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import services.ConsoleIOService;
import services.IOService;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class ConsoleIOServiceTest {
    private static final String SEPARATOR = System.lineSeparator();
    private static final String TEXT_1 = "Java is the best";
    private static final String TEXT_2 = "Java is the beast";
    private PrintStream backup;
    private ByteArrayOutputStream bos;
    private IOService ioService;

    @BeforeMethod
    void start(){
        backup = System.out;
        bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        ioService = new ConsoleIOService();
    }

    @AfterMethod
    void stop(){
        System.setOut(backup);
    }

    @Test(testName = "ConsoleServiceTest = " + TEXT_1)
    void shouldPrintFirstString() throws InterruptedException{
        ioService.print(TEXT_1);
        Thread.sleep(1000);
        Assert.assertEquals(bos.toString(), TEXT_1+SEPARATOR);
    }

    @Test(testName = "ConsoleServiceTest = " + TEXT_2)
    void shouldPrintSecondString() throws InterruptedException{
        ioService.print(TEXT_2);
        Thread.sleep(1000);
        Assert.assertEquals(bos.toString(), TEXT_2+SEPARATOR);
    }


}
