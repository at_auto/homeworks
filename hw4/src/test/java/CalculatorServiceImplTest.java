import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import services.CalculatorService;
import services.CalculatorServiceImpl;
import services.ConsoleIOService;
import services.IOService;


public class CalculatorServiceImplTest {
    private IOService ioService = new ConsoleIOService();
    private CalculatorService calculatorService;

    @DataProvider(name = "positiveData")
    public Object[][] positiveData() {
        return new Object[][]{
                new String[]{"2", "2", "+", "4"},//d1, d2, operation, expected
                new String[]{"45", "45", "-", "0"},
                new String[]{"2147483647", "2147483647", "/", "1"},
                new String[]{"2147483647", "-2147483647", "/", "-1"},
                new String[]{"2147483647", "-2147483647", "+", "0"},
                new String[]{"2147483647", "1", "*", "2147483647"},
                new String[]{"0", "1", "*", "0"},
        };
    }

    @DataProvider(name = "negativeData")
    public Object[][] negativeData() {
        return new Object[][]{
                new String[]{"564", "0", "/"},
                new String[]{"x", "y", "+"},
                new String[]{"1", "y", "-"},
                new String[]{"x", "3", "*"},
                new String[]{"", "3", "*"},
                new String[]{null, "3", "*"},
                new String[]{null, null, "-"},
                new String[]{null, null, null},
        };
    }

    @BeforeMethod
    public void startTest() {
        ioService = Mockito.mock(IOService.class);
        calculatorService = new CalculatorServiceImpl(ioService);
    }

    @Test(testName = "Тест калькулятора на операции", dataProvider = "positiveData")
    public void calculatePositiveTest(String[] data) {
        Mockito.when(ioService.readString()).thenReturn(data[0], data[1], data[2]);
        Assert.assertEquals(calculatorService.calculate(), Integer.parseInt(data[3]));
    }

    @Test(testName = "Тест калькулятора на исключения",
            dataProvider = "negativeData",
            expectedExceptions = {NumberFormatException.class, ArithmeticException.class})
    public void calculateWithExceptionsTest(String[] data) {
        Mockito.when(ioService.readString()).thenReturn(data[0], data[1], data[2]);
        calculatorService.calculate();
    }

}
