import services.CalculatorService;
import services.CalculatorServiceImpl;
import services.ConsoleIOService;
import services.IOService;

public class Main {
    public static void main(String[] args){
        IOService ioService = new ConsoleIOService();
        CalculatorService calculatorService = new CalculatorServiceImpl(ioService);
        calculatorService.calculate();
    }
}
