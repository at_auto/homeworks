package services;

public interface IOService {
    void print(String message);

    String readString();
}
