package services;

public class CalculatorServiceImpl implements CalculatorService {
    private IOService ioService;

    public CalculatorServiceImpl(IOService ioService) {
        this.ioService = ioService;
    }

    @Override
    public int calculate() {
        ioService.print("Введите первое число:");
        int d1 = Integer.parseInt(ioService.readString());
        ioService.print("Введите второе число:");
        int d2 = Integer.parseInt(ioService.readString());
        ioService.print("Введите команду:");
        int d3 = 0;
        switch (ioService.readString()) {
            case "+":
                d3 = d1 + d2;
                ioService.print(String.format("%d + %d = %d", d1, d2, d3));
                break;
            case "-":
                d3 = d1 - d2;
                ioService.print(String.format("%d - %d = %d", d1, d2, d3));
                break;
            case "/":
                d3 = d1 / d2;
                ioService.print(String.format("%d / %d = %d", d1, d2, d3));
                break;
            case "*":
                d3 = d1 * d2;
                ioService.print(String.format("%d * %d = %d", d1, d2, d3));
                break;
            default:
                ioService.print("Я не знаю такой команды. Повторите ввод.");
        }
        return d3;
    }
}
