package services;


import java.io.PrintStream;
import java.util.Scanner;

public class ConsoleIOService implements IOService{
    private final PrintStream out;
    private final Scanner sc;


    public ConsoleIOService() {
        this.out = System.out;
        this.sc = new Scanner(System.in);
    }

    @Override
    public void print(String message) {
        out.println(message);
    }

    @Override
    public String readString() {
        return sc.nextLine();
    }

}
