package services;

public interface CalculatorService {
    int calculate();
}
