package model;

import com.google.gson.annotations.SerializedName;

public class User {
	@SerializedName("firstName")
	private String firstName;
	@SerializedName("lastName")
	private String lastName;
	@SerializedName("password")
	private String password;
	@SerializedName("userStatus")
	private int userStatus;
	@SerializedName("phone")
	private String phone;
	@SerializedName("id")
	private int id;
	@SerializedName("email")
	private String email;
	@SerializedName("username")
	private String username;

	public String getFirstName(){
		return firstName;
	}

	public String getLastName(){
		return lastName;
	}

	public String getPassword(){
		return password;
	}

	public int getUserStatus(){
		return userStatus;
	}

	public String getPhone(){
		return phone;
	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public String getUsername(){
		return username;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "User{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", password='" + password + '\'' +
				", userStatus=" + userStatus +
				", phone='" + phone + '\'' +
				", id=" + id +
				", email='" + email + '\'' +
				", username='" + username + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;

		User user = (User) o;

		if (userStatus != user.userStatus) return false;
		if (id != user.id) return false;
		if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
		if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
		if (password != null ? !password.equals(user.password) : user.password != null) return false;
		if (phone != null ? !phone.equals(user.phone) : user.phone != null) return false;
		if (email != null ? !email.equals(user.email) : user.email != null) return false;
		return username != null ? username.equals(user.username) : user.username == null;
	}

	@Override
	public int hashCode() {
		int result = firstName != null ? firstName.hashCode() : 0;
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + userStatus;
		result = 31 * result + (phone != null ? phone.hashCode() : 0);
		result = 31 * result + id;
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (username != null ? username.hashCode() : 0);
		return result;
	}
}
