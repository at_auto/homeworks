package model;

import com.google.gson.annotations.SerializedName;

public class Order {
    @SerializedName("id")
    private int id;

    @SerializedName("petId")
    private int petId;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("shipDate")
    private String shipDate;

    @SerializedName("status")
    private OrderStatus status;

    @SerializedName("complete")
    private boolean complete;

    public int getId() {
        return id;
    }

    public Order setId(int id) {
        this.id = id;
        return this;
    }

    public int getPetId() {
        return petId;
    }

    public Order setPetId(int petId) {
        this.petId = petId;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public Order setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public String getShipDate() {
        return shipDate.toString();
    }

    public Order setShipDate(String shipDate) {
        this.shipDate = shipDate;
        return this;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public Order setStatus(OrderStatus status) {
        this.status = status;
        return this;
    }

    public boolean isComplete() {
        return complete;
    }

    public Order setComplete(boolean complete) {
        this.complete = complete;
        return this;
    }

    @Override
    public String toString() {
        return "model.Order{" +
                "id=" + id +
                ", petId=" + petId +
                ", quantity=" + quantity +
                ", shipDate=" + shipDate +
                ", status=" + status +
                ", complete=" + complete +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (petId != order.petId) return false;
        if (quantity != order.quantity) return false;
        if (complete != order.complete) return false;
        return status == order.status;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + petId;
        result = 31 * result + quantity;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (complete ? 1 : 0);
        return result;
    }
}
