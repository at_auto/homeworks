public final class Endpoints {
    public static String baseURL = "http://213.239.217.15:9090/api/v3";
    public static String store = "/store";
    public static String order = "/order";
    public static String orderId = "/{orderId}";
    public static String inventory = "/inventory";
    public static String user = "/user";
}
