import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import model.User;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Random;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class HomeTaskApiTestWithoutPreparing {
    private final int ID = getRandom();
    private final String api_key = "special-key"; //должен быть в проперти
    int getRandom(){
        return new Random().nextInt(1000);
    }

    @Test
    public void checkCreateAndGetUser() throws Exception{
        User user  = new User();
        user.setEmail("email" + getRandom() + "@xyz.dot");
        user.setId(ID);
        user.setUserStatus(1);
        user.setUsername("admin_" + getRandom());
        user.setFirstName("User_Name_"+ getRandom());
        user.setLastName("User_LastName_" + getRandom());
        user.setPassword("1234" + getRandom());
        user.setPhone("895894" + getRandom());
        given().baseUri(Endpoints.baseURL)
                .header(new Header("api_key", api_key))
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .log().all()
                .post(Endpoints.user)
                .then()
                .log().all()
                .statusCode(200);
        User actual = given()
                .baseUri(Endpoints.baseURL)
                .header(new Header("api_key", api_key))
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .when()
                .log().all()
                .get(Endpoints.user + "/" + user.getUsername())
                .then()
                .statusCode(200)
                .log().all()
                .extract().body().as(User.class);
        Assert.assertEquals(user, actual);
    }

}
