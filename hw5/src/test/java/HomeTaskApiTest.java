import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import model.Order;
import model.OrderStatus;
import model.User;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Random;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class HomeTaskApiTest {
    private int idToDel;
    private int idToCheck;
    private Order orderForDelete;
    private Order orderToCheck;

    @BeforeClass
    public void prepare() {
        String api_key = "special-key"; // должен быть в проперти
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(Endpoints.baseURL + Endpoints.store)
                .addHeader("api_key", api_key)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        RestAssured.filters(new ResponseLoggingFilter());
        idToDel = new Random().nextInt(500000);
        idToCheck = new Random().nextInt(500000);
        orderForDelete = new Order();
        orderForDelete
                .setId(idToDel)
                .setPetId(idToDel)
                .setQuantity(0)
                .setComplete(true)
                .setStatus(OrderStatus.delivered)
                .setShipDate(LocalDateTime.now().toString());

        orderToCheck = new Order();
        orderToCheck.setId(idToCheck).setPetId(idToCheck).setQuantity(0).setComplete(true)
                .setStatus(OrderStatus.placed)
                .setShipDate(LocalDateTime.now().toString());

        given().body(orderForDelete).when().post(Endpoints.order);
        given().body(orderToCheck).when().post(Endpoints.order);
        //отдельный заказ чтоб не связывать тесты

    }

    @Test
    public void orderSave() {
        Order orderToSave = new Order();
        int idToSave = new Random().nextInt(500000);
        orderToSave
                .setId(idToSave)
                .setPetId(idToSave)
                .setQuantity(0)
                .setComplete(true)
                .setStatus(OrderStatus.delivered)
                .setShipDate(LocalDateTime.now().toString());

        given()
                .body(orderToSave)
                .when()
                .post(Endpoints.order)
                .then()
                .statusCode(200);
    }

    @Test
    public void orderCheck() {
        Order actual =
                given()
                        .pathParam("orderId", idToCheck)
                        .when()
                        .get(Endpoints.order + Endpoints.orderId)
                        .then()
                        .statusCode(200)
                        .extract().body()
                        .as(Order.class);
        Assert.assertEquals(actual, orderToCheck);
    }

    @Test
    public void checkOrderDelete() {
        given()
                .pathParam("orderId", idToDel)
                .when()
                .delete(Endpoints.order + Endpoints.orderId)
                .then()
                .statusCode(200);
        String result = given()
                .pathParam("orderId", idToDel)
                .when()
                .delete(Endpoints.order + Endpoints.orderId)
                .then()
                .statusCode(200).extract().body().asString();
        Assert.assertEquals(result, "");
    }

    @Test()
    public void validateInventory() {
        Map inventoryResult = given()
                .when()
                .get(Endpoints.inventory)
                .then()
                .statusCode(200)
                .extract().body()
                .as(Map.class);
        Assert.assertTrue(inventoryResult.containsKey("delivered"), "Поле delivered отсутствует");
    }
}
