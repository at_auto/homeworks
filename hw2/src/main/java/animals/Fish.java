package animals;

public class Fish extends Herbivore implements Swim{

    public Fish(String name, int stomachMax) {
        super(name, stomachMax);
    }

    @Override
    public void swim() {
        System.out.println("Fish is swimming");
    }
}
