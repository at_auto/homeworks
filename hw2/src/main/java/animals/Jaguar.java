package animals;

public class Jaguar extends Carnivorous implements Run, Voice{
    public Jaguar(String name, int stomachMax) {
        super(name, stomachMax);
    }

    @Override
    public void run() {
        System.out.println("Jaguar is running fast");
    }

    @Override
    public String voice() {
        return "Grrrowl";
    }
}
