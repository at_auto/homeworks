package animals;

public class Elephant extends Herbivore implements Run, Voice, Swim{

    public Elephant(String name, int stomachMax) {
        super(name, stomachMax);
    }

    @Override
    public void run() {
        System.out.println("Elephant is running");
    }

    @Override
    public void swim() {
        System.out.println("Elephant is swimming");
    }

    @Override
    public String voice() {
        return "Tow-tow";
    }
}
