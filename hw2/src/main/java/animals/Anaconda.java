package animals;

public class Anaconda extends Carnivorous implements Run, Swim, Voice{

    public Anaconda(String name, int stomachMax) {
        super(name, stomachMax);
    }

    @Override
    public void run() {
        System.out.println("Anaconda is running to you");
    }

    @Override
    public void swim() {
        System.out.println("Anaconda is swimming");
    }

    @Override
    public String  voice() {
        return "Hissss";
    }
}
