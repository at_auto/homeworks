package animals;

import food.Food;
import food.Meat;
public class Herbivore extends Animal {
    public Herbivore(String name, int stomachMax) {
        super(name, stomachMax);
    }

    @Override
    public void eat(Food food) {
        if(food instanceof Meat){
            System.out.printf("%s doesn't eat a meat \n", this.getName());
        } else{
            if(!isFull()) {
                stomachIncrement(food.getPart());
            }
        }
    }
}
