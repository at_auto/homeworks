package animals;

public class Wolf extends Carnivorous implements Run, Swim, Voice{
    public Wolf(String name, int stomachMax) {
        super(name, stomachMax);
    }

    @Override
    public void run() {
        System.out.println("Wolf is running");
    }

    @Override
    public void swim() {
        System.out.println("Wolf is swimming");
    }

    @Override
    public String voice() {
        return "Howl-howl";
    }
}
