package animals;

import food.Food;

public abstract class Animal {
    private String name;

    private int stomach = 0;

    private int stomachMax;

    private boolean isFull = false;

    public String getName() {
        return name;
    }

    public Animal(String name, int stomachMax){
        this.name = name;
        this.stomachMax = stomachMax;
    }

    public abstract void eat(Food food);

    public void stomachIncrement(int part){
        String message ="";
        for (int i = 0; i < part; i++) {
            if(stomach<stomachMax){
                stomach++;
                message = " is eating, his stomach is full on " + stomach * 10 + "%";
            }
            else{
                isFull=true;
                message = " is full";
            }
        }
        System.out.println(name + message);
    }

    public boolean isFull(){
        return isFull;
    }
}