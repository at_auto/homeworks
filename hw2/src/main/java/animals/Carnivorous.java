package animals;

import food.Food;
import food.Grass;

public class Carnivorous extends Animal{
    public Carnivorous(String name, int stomachMax) {
        super(name, stomachMax);
    }

    @Override
    public void eat(Food food) {
        if(food instanceof Grass){
            System.out.printf("%s doesn't eat gras\n", this.getName());
        } else{
            if(!isFull()) {
                stomachIncrement(food.getPart());
            }
        }
    }
}
