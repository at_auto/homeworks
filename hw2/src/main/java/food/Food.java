package food;

public abstract class Food {
    private int part;

    public int getPart() {
        return part;
    }

    public Food(int part){
        this.part = part;
    }
}
