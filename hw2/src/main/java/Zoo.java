import animals.*;
import food.Grass;
import food.Meat;

import java.util.ArrayList;
import java.util.List;

public class Zoo {
    public static void main(String[] args) {
        Anaconda anaconda = new Anaconda("Gorinich", 3);
        Duck duck = new Duck("Scrudzh Mac-Duck", 3);
        Elephant elephant = new Elephant("Slon", 10);
        Fish fish = new Fish("Nemo", 2);
        Jaguar jaguar = new Jaguar("SuperCat", 6);
        Wolf wolf = new Wolf("Wolfenshtain", 6);
        Grass grass = new Grass(2);
        Meat meat = new Meat(2);

        Worker worker = new Worker();
        worker.feed(jaguar, meat);
        worker.feed(jaguar, meat);
        worker.feed(jaguar, meat);
        worker.feed(anaconda, grass);
        worker.feed(anaconda, meat);
        worker.feed(elephant, meat);
        worker.feed(duck, grass);
        worker.feed(duck, grass);
        worker.feed(duck, grass);
        worker.feed(duck, grass);
        for (int i = 0; i < 11; i++) {
            worker.feed(wolf, meat);
        }

        worker.getVoice(anaconda);
        worker.getVoice(wolf);

        System.out.println();

        Swim[] pond = new Swim[]{duck, anaconda, elephant, wolf, fish};

        for (Swim swim : pond) {
            swim.swim();
        }
    }
}
