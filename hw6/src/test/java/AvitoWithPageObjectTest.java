/*
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Attachment;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import pages.AvitoMainPage;
import types.CategoryTypes;
import types.SortTypes;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AvitoWithPageObjectTest {
    private AvitoMainPage avitoMainPage = new AvitoMainPage();
    private WebDriver driver;

    private JavascriptExecutor js;

    @BeforeAll
    public void setUp() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
        Configuration.baseUrl = "https://avito.ru";
        avitoMainPage.open();
        js = ((JavascriptExecutor) Selenide.webdriver().driver().getWebDriver());
        driver = Selenide.webdriver().driver().getWebDriver();
        driver.manage().window().maximize();
    }

    @Test
    @Order(1)
    public void checkCategory() {
        avitoMainPage.chooseCategory(CategoryTypes.Оргтехника_и_расходники).checkCategory();
        captureScreenshot();
    }

    @Test
    @Order(2)
    public void inputGood() {
        avitoMainPage.searchGood("Принтер").checkGood();
        captureScreenshot();
    }

    @Test
    @Order(3)
    public void checkDelivery() {
        avitoMainPage.checkDelivery();
        captureScreenshot();
    }

    @Test
    @Order(4)
    public void clickCity() {
        avitoMainPage.clickCity();
        captureScreenshot();
    }

    @Test
    @Order(5)
    public void setCity() {
        avitoMainPage.setCity("Владивосток");
        captureScreenshot();
    }

    @Test
    @Order(6)
    public void clickCityBtn() {
        avitoMainPage.clickCityBtn();
        captureScreenshot();
    }

    @Test
    @Order(7)
    public void checkResults() {
        avitoMainPage.checkResults();
        captureScreenshot();
    }

    @Test
    @Order(8)
    public void setSort() {
        avitoMainPage.setSort(SortTypes.Дороже);
        captureScreenshot();
    }

    @Test
    @Order(9)
    public void printGoods() {
        avitoMainPage.printGoods(3);
        captureScreenshot();
    }

    @Attachment
    public byte[] captureScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}

 */
