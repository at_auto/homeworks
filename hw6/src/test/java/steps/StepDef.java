package steps;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.Дано;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import io.qameta.allure.Attachment;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import pages.AvitoMainPage;
import types.CategoryTypes;
import types.SortTypes;

import java.util.List;


public class StepDef {
    private final AvitoMainPage avitoMainPage = new AvitoMainPage();
    private WebDriver driver;
    private JavascriptExecutor js;
    private List<List<String>> parameters;

    @Before
    public void setUp(){
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
        Configuration.baseUrl = "https://avito.ru";
        avitoMainPage.open();
        js = ((JavascriptExecutor) Selenide.webdriver().driver().getWebDriver());
        driver = Selenide.webdriver().driver().getWebDriver();
        driver.manage().window().maximize();
    }

    @ParameterType(".*")
    public CategoryTypes categoryTypes(String type){
        return CategoryTypes.valueOf(type);
    }

    @ParameterType(".*")
    public SortTypes sortTypes(String type){
        return SortTypes.valueOf(type);
    }

    @Дано("^параметры для ресурса$")
    public void saveParams(DataTable args){
        parameters = args.asLists(String.class);
        System.out.println("Good = " + parameters.get(0).get(1) + " ");
        System.out.println("City = " + parameters.get(1).get(1) + " ");
        System.out.println("Value = " + parameters.get(2).get(1) + " ");
    }


    @Пусть("открыт ресурс авито")
    public void open(){

    }

    @И("в выпадающем списке категорий выбрана {categoryTypes}")
    public void checkCategory(CategoryTypes category) {
        avitoMainPage.chooseCategory(category).checkCategory();
        captureScreenshot();
    }

    @И("в поле поиска введено значение принтер")
    public void inputGood() {
        avitoMainPage.searchGood(parameters.get(0).get(1)).checkGood();
        captureScreenshot();
    }

    @И("активирован чекбокс только с фотографией")
    public void checkPhoto(){
        avitoMainPage.checkPhoto();
        captureScreenshot();
    }

    @Тогда("кликнуть по выпадающему списку региона")
    public void clickRegion(){
        avitoMainPage.clickCity();
        captureScreenshot();
    }

    @Тогда("в поле регион введено значение Владивосток")
    public void setCity() {
        avitoMainPage.setCity(parameters.get(1).get(1));
        captureScreenshot();
    }

    @И("нажата кнопка показать объявления")
    public void clickCityBtn() {
        avitoMainPage.clickCityBtn();
        captureScreenshot();
    }

    @Тогда("открылась страница результаты по запросу принтер")
    public void checkResults() {
        avitoMainPage.checkResults();
        captureScreenshot();
    }

    @И("в выпадающем списке сортировка выбрано значение {sortTypes}")
    public void setSort(SortTypes type) {
        avitoMainPage.setSort(type);
        captureScreenshot();
    }

    @И("в консоль выведено значение названия и цена 3 первых товаров")
    public void printGoods() {
        avitoMainPage.printGoods(Integer.parseInt(parameters.get(2).get(1)));
        captureScreenshot();
    }

    @Attachment
    public byte[] captureScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}
