package types;

public enum SortTypes {
    Дороже("2"),
    Дешевле("1"),
    По_умолчанию("101"),
    По_дате("14");

    private String code;
    SortTypes(String code) {
        this.code = code;
    }

    public String getCode(){
        return code;
    }

    public String getName(){
        return this.name();
    }
}
