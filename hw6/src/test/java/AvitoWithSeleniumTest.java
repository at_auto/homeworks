import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AvitoWithSeleniumTest {
    private WebDriver driver = new ChromeDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 5);

    public void connect() {
        System.setProperty("webdriver.chrome.driver", "D:\\qa\\intership\\chd.exe");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://avito.ru");
    }

    public void setCategory() {
        WebElement categoryElement = driver.findElement(By.id("category"));
        Select categorySelect = new Select(categoryElement);
        categorySelect.selectByValue("99");
    }

    public void setPrinter() {
        wait.until(ExpectedConditions.presenceOfElementLocated(
                By.cssSelector("div[class^=\"suggest-inputWrap\"] input"))).sendKeys("Принтер");
        driver.findElement(
                By.cssSelector("[data-marker=\"search-form/region\"]")).click();
    }


    public void setCity() {
        wait.until(ExpectedConditions.presenceOfElementLocated(
                By.cssSelector("div[class^=\"popup-city\"] input"))).sendKeys("Владивосток");
        driver.findElement(
                By.cssSelector("ul[class^=suggest-suggests] span strong")).click();
        driver.findElement(
                By.cssSelector("[data-marker=\"popup-location/save-button\"]")).click();
    }

    public void setDelivery() {
        WebElement deliveryCheckbox = driver.findElement(
                By.cssSelector("[class^=checkbox-input][data-marker=\"delivery-filter/input\"]"));
        if (!deliveryCheckbox.isSelected()) {
            driver.findElement(
                    By.cssSelector("span[data-marker=\"delivery-filter/text\"]")).click();
        }
        driver.findElement(
                By.cssSelector("[class^=\"applyButton-box\"] [data-marker=\"search-filters/submit-button\"]")).click();
    }

    public void printGoods(){
        WebElement priceSortElement = driver.findElement(
                By.cssSelector("[class^=\"select-select-box\"] [class^=\"select-select\"]"));
        Select priceSortSelect = new Select(priceSortElement);
        priceSortSelect.selectByValue("2");
        for (int i = 0; i < 3; i++) {
            System.out.println(
                    "Title - " + driver.findElements(By.xpath("//div/div/a/h3")).get(i).getText() + "\n" +
                    "price - " + driver.findElements(By.xpath("//div/span/span/span")).get(i).getText() + "\n" +
                    "----------------------------------------------------------------------------");
        }
    }

    public void quit(){
        driver.quit();
    }
}
