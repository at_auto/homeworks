package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import types.*;

public class AvitoMainPage {
    private String categoryCode = "";
    private String good = "";
    private final SelenideElement CATEGORY = $("#category");
    private final SelenideElement IS_CATEGORY = $("option[value=\"" + categoryCode + "\"]");
    private final SelenideElement MAIN_SEARCH_ADV_FIELD = $("[data-marker=\"search-form/suggest\"]");
    private final SelenideElement CITY_FIELD = $(By.xpath("//*[@id=\"app\"]/div[2]/div/div[2]/div/div[5]/div[1]/span/span/div/div"));
    private final SelenideElement SEARCH_CITY_FIELD = $(By.xpath("//*[@id=\"app\"]/div[2]/div/div[2]/div/div[6]/div/div/span/div/div[1]/div[2]/div/input"));
    private final SelenideElement CITY_SUGGEST = $(By.xpath("//*[@id=\"app\"]/div[2]/div/div[2]/div/div[6]/div/div/span/div/div[1]/div[2]/div/ul/li[1]/span/span"));
    private final SelenideElement CITY_BUTTON = $("button[data-marker=\"popup-location/save-button\"]");
    private final SelenideElement DELIVERY_CHECKBOX = $("input[data-marker=\"delivery-filter/input\"]");
    private final SelenideElement DELIVERY_SET_CHECKBOX = $("span[data-marker=\"delivery-filter/text\"]");
    private final SelenideElement SEARCH_BUTTON = $(By.xpath("//*[@id=\"app\"]/div[2]/div/div[2]/div/div[3]/button"));
    private final SelenideElement CHECK_RESULTS = $(By.xpath("//*[@id=\"app\"]/div[3]/div[2]/div/h1"));
    private final ElementsCollection GOOD_TITLE = $$(By.xpath("//div/div/a/h3"));
    private final ElementsCollection GOOD_PRICE = $$(By.xpath("//div/span/span/span"));
    private final SelenideElement CHECK_PHOTO = $("[data-marker=\"search-form/with-images\"]");
    private final SelenideElement CHECK_PHOTO_FIELD = $(By.xpath("//*[@id=\"app\"]/div[2]/div/div[2]/div/div[4]/div[1]/label[2]/span"));
    private final SelenideElement SORT_ADV = $(By.xpath("//*[@id=\"app\"]/div[3]/div[3]/div[3]/div[1]/div[2]/select")); // 2 дороже

    @Step("Открытие главной страницы")
    public AvitoMainPage open() {
        Selenide.open("/");
        return this;
    }

    @Step("Выбор категории: {category}")
    public AvitoMainPage chooseCategory(CategoryTypes category) {
        categoryCode = category.getCode();
        CATEGORY.selectOptionByValue(categoryCode);
        return this;
    }

    public AvitoMainPage checkCategory() {
        IS_CATEGORY.isSelected();
        return this;
    }

    @Step("Поиск товара: {good}")
    public AvitoMainPage searchGood(String good) {
        this.good = good;
        MAIN_SEARCH_ADV_FIELD.shouldBe(Condition.visible).sendKeys(good);
        SEARCH_BUTTON.click();
        return this;
    }

    public AvitoMainPage checkGood() {
        MAIN_SEARCH_ADV_FIELD.shouldHave(Condition.value(good));
        return this;
    }

    @Step("Проверка, что чекбокс доставки включен")
    public AvitoMainPage checkDelivery() {
        if (!DELIVERY_CHECKBOX.scrollTo().isSelected()) {
            DELIVERY_SET_CHECKBOX.scrollTo().click();
        }
        return this;
    }

    public AvitoMainPage checkPhoto() {
        if (!CHECK_PHOTO.isSelected()) {
            CHECK_PHOTO_FIELD.click();
        }
        return this;
    }

    @Step("Клик по списку регионов")
    public AvitoMainPage clickCity() {
        CITY_FIELD.click();
        return this;
    }

    @Step("Вводим в поле региона: {city}")
    public AvitoMainPage setCity(String city) {
        SEARCH_CITY_FIELD.sendKeys(city);
        CITY_SUGGEST.should(Condition.visible).shouldHave(Condition.text(city)).click();
        return this;
    }

    @Step("Нажимаем на кнопку Показать объявления")
    public AvitoMainPage clickCityBtn() {
        CITY_BUTTON.click();
        return this;
    }

    @Step("Проверка что результаты поиска правильные")
    public AvitoMainPage checkResults() {
        CHECK_RESULTS.shouldHave(Condition.text("Объявления по запросу «" + good + "» во Владивостоке"));
        return this;
    }

    @Step("Включение сортировки по типу: {type}")
    public AvitoMainPage setSort(SortTypes type) {
        SORT_ADV.selectOptionByValue(type.getCode());
        return this;
    }

    @Step("Вывод {value} товаров в  консоль")
    public void printGoods(int value) {
        for (int i = 0; i < value; i++) {
            System.out.println(
                    "Title - " + GOOD_TITLE.get(i).getText() + "\n" +
                            "price - " + GOOD_PRICE.get(i).getText() + "\n" +
                            "----------------------------------------------------------------------------");
        }
    }

}
