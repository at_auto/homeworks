/*
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Attachment;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.Selenide.$;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AvitoWithSelenideTest {
    private JavascriptExecutor js = ((JavascriptExecutor) Selenide.webdriver().driver().getWebDriver());


    @BeforeAll
    public static void setUp(){
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
        Selenide.open("https://avito.ru");
        Selenide.webdriver().driver().getWebDriver().manage().window().maximize();
    }

    @Test
    @DisplayName("Выбор категории оргтехники")
    @Order(2)
    public void setCategory(){
        $(By.id("category")).selectOptionByValue("99");
        $("[class^=\"rubricator-list-item-link\"][data-category-id=\"99\"]").shouldHave(Condition.text("Оргтехника и расходники"));
    }

    @Test
    @DisplayName("Выбор города Владивосток")
    @Order(1)
    public void setCity(){
        $("[data-marker=\"search-form/region\"]").click(); // Кликаем на поле региона
        $("div[class^=\"popup-city\"] input").sendKeys("Владивосток"); // вводим владик
        $("ul[class^=suggest-suggests] span strong").shouldBe(Condition.exist).click(); // кликаем на владик, только после того как он появится
        $("[data-marker=\"popup-location/save-button\"]").click();// кликаем на кнопку показать
        js.executeScript("window.scrollIntoView"); // нужен скролл вниз если элемент скрыт

        $("span[class^=\"geo-address-\"]").shouldBe(Condition.text("Владивосток")); // проверяем город в первом объявлении
    }

    @Test
    @DisplayName("Проверка и активация доставки")
    @Order(3)
    public void checkDelivery(){
        js.executeScript("window.scrollBy(0, 600)"); // нужен скролл вниз если элемент скрыт
        if(!$("[class^=checkbox-input][data-marker=\"delivery-filter/input\"]").isSelected()){ // доставка выключена?
            $("span[data-marker=\"delivery-filter/text\"]").click(); // включаем
        }
        $("[class^=\"applyButton-box\"] [data-marker=\"search-filters/submit-button\"]").click();  //  показать объявления
        js.executeScript("window.scrollIntoView"); // нужен скролл вниз если элемент скрыт
        $("[class^=checkbox-input][data-marker=\"delivery-filter/input\"]").shouldBe(Condition.selected); // чекаем что доставка есть
    }

    @Test
    @DisplayName("Вывод в консоль трех товаров подороже")
    @Order(4)
    public void printGoods(){
        $("[class^=\"select-select-box\"] [class^=\"select-select\"]").selectOptionByValue("2"); // сортировка по-дороже
        for (int i = 0; i < 3; i++) {
            System.out.println(
                    "Title - " + Selenide.$$(By.xpath("//div/div/a/h3")).get(i).getText() + "\n" +
                            "price - " + Selenide.$$(By.xpath("//div/span/span/span")).get(i).getText() + "\n" +
                            "----------------------------------------------------------------------------");
        }
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public byte[] saveScreenshot(byte[] screenShot) {
        return screenShot;
    }
}
 */
