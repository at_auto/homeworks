package animals;

import Exceptions.WrongFoodException;
import aviary.AviarySize;
import food.Food;
import food.Meat;
public class Herbivore extends Animal {
    public Herbivore(String name, int stomachMax, AviarySize aviarySize) {
        super(name, stomachMax, aviarySize);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if(food instanceof Meat){
            throw new WrongFoodException(this.getName() + " doesn't eat meat");
        } else{
            if(!isFull()) {
                stomachIncrement(food.getPart());
            }
        }
    }
}
