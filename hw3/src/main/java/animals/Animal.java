package animals;

import Exceptions.WrongFoodException;
import aviary.AviarySize;
import food.Food;

public abstract class Animal{
    private final String name;
    private int stomach = 0;
    private int stomachMax;
    private boolean isFull = false;

    private AviarySize aviarySize;

    public String getName() {
        return name;
    }

    public AviarySize getAviarySize() {
        return aviarySize;
    }

    public Animal(String name, int stomachMax, AviarySize aviarySize){
        this.name = name;
        this.stomachMax = stomachMax;
        this.aviarySize = aviarySize;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    public void stomachIncrement(int part){
        String message ="";
        for (int i = 0; i < part; i++) {
            if(stomach<stomachMax){
                stomach++;
                message = " is eating, his stomach is full on " + stomach * 10 + "%";
            }
            else{
                isFull=true;
                message = " is full";
            }
        }
        System.out.println(name + message);
    }

    public boolean isFull(){
        return isFull;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Animal)) return false;

        Animal animal = (Animal) o;

        if (stomach != animal.stomach) return false;
        if (stomachMax != animal.stomachMax) return false;
        if (isFull != animal.isFull) return false;
        if (name != null ? !name.equals(animal.name) : animal.name != null) return false;
        return aviarySize == animal.aviarySize;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + stomach;
        result = 31 * result + stomachMax;
        result = 31 * result + (isFull ? 1 : 0);
        result = 31 * result + (aviarySize != null ? aviarySize.hashCode() : 0);
        return result;
    }
}