package animals;

import aviary.AviarySize;

public class Fish extends Herbivore implements Swim{
    public Fish(String name, int stomachMax, AviarySize aviarySize) {
        super(name, stomachMax, aviarySize);
    }

    @Override
    public void swim() {
        System.out.println("Fish is swimming");
    }
}
