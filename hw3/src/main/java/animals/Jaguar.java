package animals;

import aviary.AviarySize;

public class Jaguar extends Carnivorous implements Run, Voice{
    public Jaguar(String name, int stomachMax, AviarySize aviarySize) {
        super(name, stomachMax, aviarySize);
    }

    @Override
    public void run() {
        System.out.println("Jaguar is running fast");
    }

    @Override
    public String voice() {
        return "Grrrowl";
    }
}
