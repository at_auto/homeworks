package animals;

import aviary.AviarySize;

public class Wolf extends Carnivorous implements Run, Swim, Voice{
    public Wolf(String name, int stomachMax, AviarySize aviarySize) {
        super(name, stomachMax, aviarySize);
    }

    @Override
    public void run() {
        System.out.println("Wolf is running");
    }

    @Override
    public void swim() {
        System.out.println("Wolf is swimming");
    }

    @Override
    public String voice() {
        return "Howl-howl";
    }
}
