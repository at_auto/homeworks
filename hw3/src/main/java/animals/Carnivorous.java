package animals;

import Exceptions.WrongFoodException;
import aviary.AviarySize;
import food.Food;
import food.Grass;

public class Carnivorous extends Animal{
    public Carnivorous(String name, int stomachMax, AviarySize aviarySize) {
        super(name, stomachMax, aviarySize);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if(food instanceof Grass){
            throw new WrongFoodException(this.getName() + " doesn't eat grass");
        } else{
            if(!isFull()) {
                stomachIncrement(food.getPart());
            }
        }
    }
}
