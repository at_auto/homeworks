package animals;

import aviary.AviarySize;

public class Elephant extends Herbivore implements Run, Voice, Swim{
    public Elephant(String name, int stomachMax, AviarySize aviarySize) {
        super(name, stomachMax, aviarySize);
    }

    @Override
    public void run() {
        System.out.println("Elephant is running");
    }

    @Override
    public void swim() {
        System.out.println("Elephant is swimming");
    }

    @Override
    public String voice() {
        return "Tow-tow";
    }
}
