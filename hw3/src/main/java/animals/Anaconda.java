package animals;

import aviary.AviarySize;

public class Anaconda extends Carnivorous implements Run, Swim, Voice{
    public Anaconda(String name, int stomachMax, AviarySize aviarySize) {
        super(name, stomachMax, aviarySize);
    }

    @Override
    public void run() {
        System.out.println("Anaconda is running to you");
    }

    @Override
    public void swim() {
        System.out.println("Anaconda is swimming");
    }

    @Override
    public String  voice() {
        return "Hissss";
    }
}
