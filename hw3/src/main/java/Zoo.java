import Exceptions.WrongAnimalSizeException;
import Exceptions.WrongFoodException;
import animals.*;
import aviary.Aviary;
import aviary.AviarySize;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args){
        Anaconda anaconda = new Anaconda("Gorinich", 3, AviarySize.SMALL);
        Duck duck = new Duck("Scrudzh Mac-Duck", 3, AviarySize.NORMAL);
        Elephant elephant = new Elephant("Slon", 10, AviarySize.HUGE);
        Fish fish = new Fish("Nemo", 2, AviarySize.SMALL);
        Jaguar jaguar = new Jaguar("SuperCat", 6, AviarySize.BIG);
        Wolf wolf = new Wolf("Wolfenshtain", 6, AviarySize.BIG);
        Grass grass = new Grass(2);
        Meat meat = new Meat(2);

        Worker worker = new Worker();
        try {
            worker.feed(jaguar, meat);
            worker.feed(jaguar, meat);
            worker.feed(jaguar, meat);
//        worker.feed(anaconda, grass);//exception
            worker.feed(anaconda, meat);
//        worker.feed(elephant, meat);
            worker.feed(duck, grass);
            worker.feed(duck, grass);
            worker.feed(duck, grass);
            worker.feed(duck, grass);
            for (int i = 0; i < 11; i++) {
                worker.feed(wolf, meat);
            }

        }  catch (WrongFoodException e){
            System.out.println(e.getMessage());
        }
        worker.getVoice(anaconda);
        worker.getVoice(wolf);

        System.out.println();//backspace

        Swim[] pond = new Swim[]{duck, anaconda, elephant, wolf, fish};

        for (Swim swim : pond) {
            swim.swim();
        }

        System.out.println();//backspace
        Aviary<Carnivorous> carnivorousAviary = new Aviary<>(AviarySize.SMALL);

        try{
            carnivorousAviary.addAnimal(wolf);
            carnivorousAviary.addAnimal(jaguar);
            System.out.println("carnivorous aviary size is " + carnivorousAviary.getSize());
            System.out.println("get by name " + carnivorousAviary.getAnimalByName("SuperCat").getName());
        } catch (WrongAnimalSizeException e){
            System.out.println(e.getMessage());
        } catch (NullPointerException e){
            System.out.println("Oops. We have null");
        }


        Aviary<Herbivore> herbivoreAviary = new Aviary<>(AviarySize.BIG);
        try {
            herbivoreAviary.addAnimal(elephant);//exception
            herbivoreAviary.addAnimal(fish);
            herbivoreAviary.addAnimal(fish);
            herbivoreAviary.addAnimal(duck);
            herbivoreAviary.addAnimal(duck);
            herbivoreAviary.addAnimal(duck);
            herbivoreAviary.addAnimal(duck);
        } catch (WrongAnimalSizeException e){
            System.out.println(e.getMessage());
        }
        herbivoreAviary.removeAnimal(elephant);
        System.out.println("herbivore aviary size is " + herbivoreAviary.getSize());

        herbivoreAviary.removeAnimal(elephant);

    }
}
