package aviary;

public enum AviarySize {
    HUGE(4),
    BIG(3),
    NORMAL(2),
    SMALL(1);
    private int weight;

    public int getWeight(){
        return weight;
    }

    AviarySize(int weight){
        this.weight = weight;
    }

}