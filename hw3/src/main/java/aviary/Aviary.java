package aviary;

import Exceptions.WrongAnimalSizeException;
import animals.Animal;

import java.util.HashSet;
import java.util.Set;

public class Aviary<T extends Animal> {
    private Set<T> aviarySet = new HashSet<>();
    private AviarySize aviarySize;

    public Aviary(AviarySize aviarySize){
        this.aviarySize = aviarySize;
    }

    public void addAnimal(T animal) throws WrongAnimalSizeException {
        if(animal.getAviarySize().getWeight()<=aviarySize.getWeight())
        aviarySet.add(animal);
        else throw new WrongAnimalSizeException("Wrong animal size");
    }

    public T getAnimalByName(String name){
        T t = null;
        for(T animal : aviarySet){
            if(animal.getName().equals(name)) t = animal;
        }
        return t;
    }

    public void removeAnimal(T t){
        aviarySet.remove(t);
    }

    public int getSize(){
        return aviarySet.size();
    }
}
